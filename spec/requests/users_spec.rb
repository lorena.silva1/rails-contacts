 require 'rails_helper'

 #using shoulda
class UsersController < ApplicationController
  RSpec.describe UsersController, type: :controller do
    context 'when route get to index' do
      it { expect route(:get, 'users').to(action: :index) }
    end
  end
end
