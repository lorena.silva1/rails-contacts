Rails.application.routes.draw do
  get 'users' , to: 'users#index'
  get '/users/:id' , to: 'users#show', as: 'user'
  get 'users' , to: 'users#new'
  get 'users' , to: 'users#create'
  get 'users' , to: 'users#edit'
  get 'users' , to: 'users#update'
  get 'users' , to: 'users#destroy'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
